
"""""""""""""""""""""""""""""""""""""""""""""""""
"" vim settings
"""""""""""""""""""""""""""""""""""""""""""""""""

set nocompatible
syntax on

" Files
set autoread
set confirm
filetype plugin on

" Key Operations
set tildeop

set backspace=indent,eol,start

" Terminal
set ttyfast

set splitbelow
set splitright

set lazyredraw

" Status Bar
set visualbell

set wildmenu
set wildmode=full

set completeopt=menu

set inccommand=nosplit

set showcmd
set showmatch
set showmode
set noshowmode

" Formatting
set autoindent

set smarttab
set expandtab
set tabstop=2
set shiftwidth=2

set ignorecase
set smartcase
set infercase

set synmaxcol=120

" Search
set hlsearch
set incsearch

" Markers
set cursorline
set ruler

set nowrap
set number
set relativenumber

" System
set clipboard=unnamed
set updatetime=300
