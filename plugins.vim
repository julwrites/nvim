"""""""""""""""""""""""""""""""""""""""""""""""""
"" plugin settings
"""""""""""""""""""""""""""""""""""""""""""""""""
" CoC
runtime plug-config/coc.vim
let g:coc_global_extensions=[
      \'coc-marketplace',
      \'coc-python',
      \'coc-rls',
      \'coc-vetur',
      \'coc-cmake',
      \'coc-json',
      \'coc-yaml',
      \'coc-eslint',
      \'coc-tsserver',
      \]

" Ale
let g:ale_disable_lsp = 1
let g:ale_linters_explicit = 1
let g:ale_sign_column_always = 1
let g:ale_fix_on_save = 1
let b:ale_linters = {'javascript': ['eslint'],'swift': ['swiftlint']}
let g:ale_fixers = {
                  \   '*': ['remove_trailing_lines', 'trim_whitespace'],
                  \   'javascript': ['eslint'],
                  \}
let g:ale_completion_enabled = 1
let g:ale_floating_preview = 1

" Autoformat
let g:autoformat_autoindent = 0
let g:autoformat_retab = 0
let g:autoformat_remove_trailing_spaces = 1

" Omnipytent
let g:omnipytent_filePrefix = '.julwrites'
let g:omnipytent_defaultPythonVersion = 3

" Vimwiki
let g:vimwiki_list = [
      \{'path': '~/julwrites/wiki/vimwiki/', 'ext': '.md'}
      \]

" Startify
let g:startify_lists = [
      \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
      \ { 'type': 'files',     'header': ['   MRU']            },
      \ { 'type': 'dir',       'header': ['   MRU '. getcwd()] },
      \ ]
let g:startify_bookmarks = ['$MYVIMRC', '$JULWRITES']

" Elm
let g:polyglot_disabled = ['elm']

" Vimtex
let g:vimtex_view_method = 'sumatrapdf'

"""""""""""""""""""""""""""""""""""""""""""""""""
"" plugins
"""""""""""""""""""""""""""""""""""""""""""""""""

set nocompatible

call plug#begin()

"" Development Environment
Plug 'neoclide/coc.nvim', {'branch': 'release', 'do': { -> coc#util#install() }}
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'wincent/ferret'
Plug 'scrooloose/nerdtree'
Plug 'itchyny/lightline.vim'
Plug 'mhinz/vim-startify'
Plug 'airblade/vim-rooter'
Plug 'voldikss/vim-floaterm'
Plug 'metakirby5/codi.vim'
Plug 'tpope/vim-eunuch'

"" Development Tooling
Plug 'CoatiSoftware/vim-sourcetrail'
Plug 'kkvh/vim-docker-tools'

"" Navigation
Plug 'easymotion/vim-easymotion'
Plug 'tpope/vim-surround'
Plug 'wellle/targets.vim'
Plug 'nelstrom/vim-visual-star-search'
Plug 'ntpeters/vim-better-whitespace'
Plug 'wellle/context.vim'

"" Language Support
Plug 'tpope/vim-commentary'
Plug 'dzeban/vim-log-syntax'
Plug 'SirVer/ultisnips'
Plug 'elmcast/elm-vim'
Plug 'sheerun/vim-polyglot'
Plug 'dense-analysis/ale'
Plug 'maximbaz/lightline-ale'
Plug 'keith/swift.vim'
Plug 'lervag/vimtex'
Plug 'hylang/vim-hy'

"" Tools
Plug 'idanarye/vim-omnipytent'
Plug 'tpope/vim-fugitive'
Plug 'christoomey/vim-conflicted'
Plug 'vimwiki/vimwiki'

"" Formatting
Plug 'tommcdo/vim-lion'
Plug 'chiel92/vim-autoformat'

" Color schemes
Plug 'joshdick/onedark.vim'
Plug 'sonph/onehalf', { 'rtp': 'vim' }

call plug#end()
