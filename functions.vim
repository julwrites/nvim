
"""""""""""""""""""""""""""""""""""""""""""""""""
"" vim functions
"""""""""""""""""""""""""""""""""""""""""""""""""

if !exists('*Refresh')
  function! Refresh()
    so $MYVIMRC
    redraw!
  endfunction
endif

if !exists('*Update')
  function! Update()
    :call Refresh()
    PlugUpgrade
    PlugInstall
    PlugUpdate
    UpdateRemotePlugins
  endfunction
endif
