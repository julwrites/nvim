
"""""""""""""""""""""""""""""""""""""""""""""""""
"" graphical settings
"""""""""""""""""""""""""""""""""""""""""""""""""

" General settings
syntax on
set cursorline
set guifont="Fira Code", Iosevka, Consolas

" NERDTree
let NERDTreeShowHidden=1

" Startify
let g:startify_custom_header = [
\   '         _/      _/  _/      _/  _/_/_/  _/      _/',
\   '        _/_/    _/  _/      _/    _/    _/_/  _/_/',
\   '       _/  _/  _/  _/      _/    _/    _/  _/  _/',
\   '      _/    _/_/    _/  _/      _/    _/      _/',
\   '     _/      _/      _/      _/_/_/  _/      _/'
\ ]

if has("win32")
  colorscheme onedark
  let g:lightline_colorscheme = 'one'
else
  colorscheme onehalfdark
  let g:lightline_colorscheme = 'onehalfdark'

  if system('uname -s') != "Darwin\n"
    "Linux
    if (has("termguicolors"))
      let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
      let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
      set termguicolors
      set t_Co=256
    endif

    colorscheme onehalfdark
    let g:lightline_colorscheme = 'onehalfdark'
  endif
endif

" Lightline
let g:lightline = {
      \ 'colorscheme': g:lightline_colorscheme,
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified'] ]
      \ },
      \ 'component_function': {
      \     'gitbranch': 'FugitiveHead'
      \ }
      \ }
