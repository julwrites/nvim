if &compatible
  set nocompatible
endif

runtime plugins.vim
runtime settings.vim
runtime gsettings.vim
runtime functions.vim
runtime bindings.vim
