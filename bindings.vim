"""""""""""""""""""""""""""""""""""""""""""""""""
"" functions
"""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""
"" keymaps
"""""""""""""""""""""""""""""""""""""""""""""""""
" Remap terminal escape to esc key
:tnoremap <Esc><Esc> <C-\><C-n>
" Display and allow selection of buffers
:nnoremap <C-b> :buffers<CR>:buffer
:nnoremap <C-k><C-w> :Startify<CR>

" Mapping time stamp to <Ctrl + t><Ctrl + s>
:nnoremap <C-t><C-s> i<C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR><Esc>
:inoremap <C-t><C-s> <C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR><Esc>

" NERDTree
map <C-n> :NERDTreeToggle<CR>

" FZF
map <C-p> :FZF<CR>

" Coc
" Bind tab to trigger completion with characters ahead
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
nmap <silent> gh <Plug>(coc-action-doHover)

" Bind ctrl space to trigger completion
inoremap <silent><expr> <C-space> coc#refresh()

map <M-s> :CocConfig<CR>

" Floaterm
if system('uname -s') == "Darwin\n"
      nnoremap   <silent>   <D-t>   :FloatermToggle<CR>
      tnoremap   <silent>   <D-t>   <C-\><C-n>:FloatermToggle<CR>
else
      nnoremap   <silent>   <M-t>   :FloatermToggle<CR>
      tnoremap   <silent>   <M-t>   <C-\><C-n>:FloatermToggle<CR>
endif

"""""""""""""""""""""""""""""""""""""""""""""""""
"" Automation
"""""""""""""""""""""""""""""""""""""""""""""""""
" Auto commands
autocmd StdinReadPre * let s:std_in=1

" Do updates
autocmd VimEnter * call Refresh()
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif

" File commands
autocmd BufWrite * Autoformat
autocmd BufNewFile,BufRead Jenkinsfile setf groovy

" NERDTree auto-close
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
